import * as plugins from './smartrequest.plugins.js';
import * as interfaces from './smartrequest.interfaces.js';

import { IncomingMessage } from 'http';

export interface IExtendedIncomingMessage extends IncomingMessage {
  body: any;
}

const buildUtf8Response = (
  incomingMessageArg: IncomingMessage,
  autoJsonParse = true
): Promise<IExtendedIncomingMessage> => {
  const done = plugins.smartpromise.defer<IExtendedIncomingMessage>();
  // Continuously update stream with data
  let body = '';
  incomingMessageArg.on('data', (chunkArg) => {
    body += chunkArg;
  });

  incomingMessageArg.on('end', () => {
    if (autoJsonParse) {
      try {
        (incomingMessageArg as IExtendedIncomingMessage).body = JSON.parse(body);
      } catch (err) {
        (incomingMessageArg as IExtendedIncomingMessage).body = body;
      }
    } else {
      (incomingMessageArg as IExtendedIncomingMessage).body = body;
    }
    done.resolve(incomingMessageArg as IExtendedIncomingMessage);
  });
  return done.promise;
};

/**
 * determine wether a url is a unix sock
 * @param urlArg
 */
const testForUnixSock = (urlArg: string): boolean => {
  const unixRegex = /^(http:\/\/|https:\/\/|)unix:/;
  return unixRegex.test(urlArg);
};

/**
 * determine socketPath and path for unixsock
 */
const parseSocketPathAndRoute = (stringToParseArg: string) => {
  const parseRegex = /(.*):(.*)/;
  const result = parseRegex.exec(stringToParseArg);
  return {
    socketPath: result[1],
    path: result[2],
  };
};

/**
 * a custom http agent to make sure we can set custom keepAlive options for speedy subsequent calls
 */
const httpAgent = new plugins.agentkeepalive({
  keepAlive: true,
  maxFreeSockets: 10,
  maxSockets: 100,
  maxTotalSockets: 1000,
  timeout: 60000,
});

/**
 * a custom http agent to make sure we can set custom keepAlive options for speedy subsequent calls
 */
const httpAgentKeepAliveFalse = new plugins.agentkeepalive({
  keepAlive: false,
  timeout: 60000,
});

/**
 * a custom https agent to make sure we can set custom keepAlive options for speedy subsequent calls
 */
const httpsAgent = new plugins.agentkeepalive.HttpsAgent({
  keepAlive: true,
  maxFreeSockets: 10,
  maxSockets: 100,
  maxTotalSockets: 1000,
  timeout: 60000,
});

/**
 * a custom https agent to make sure we can set custom keepAlive options for speedy subsequent calls
 */
const httpsAgentKeepAliveFalse = new plugins.agentkeepalive.HttpsAgent({
  keepAlive: false,
  timeout: 60000,
});

export let request = async (
  urlArg: string,
  optionsArg: interfaces.ISmartRequestOptions = {},
  responseStreamArg: boolean = false,
  requestDataFunc: (req: plugins.http.ClientRequest) => void = null
): Promise<IExtendedIncomingMessage> => {
  const done = plugins.smartpromise.defer<IExtendedIncomingMessage>();

  // merge options
  const defaultOptions: interfaces.ISmartRequestOptions = {
    // agent: agent,
    autoJsonParse: true,
    keepAlive: true,
  };

  optionsArg = {
    ...defaultOptions,
    ...optionsArg,
  };

  // parse url
  const parsedUrl = plugins.smarturl.Smarturl.createFromUrl(urlArg, {
    searchParams: optionsArg.queryParams || {},
  });
  optionsArg.hostname = parsedUrl.hostname;
  if (parsedUrl.port) {
    optionsArg.port = parseInt(parsedUrl.port, 10);
  }
  optionsArg.path = parsedUrl.path;
  optionsArg.queryParams = parsedUrl.searchParams;

  // determine if unixsock
  if (testForUnixSock(urlArg)) {
    const detailedUnixPath = parseSocketPathAndRoute(optionsArg.path);
    optionsArg.socketPath = detailedUnixPath.socketPath;
    optionsArg.path = detailedUnixPath.path;
  }

  // TODO: support tcp sockets

  // lets determine agent
  switch (true) {
    case !!optionsArg.agent:
      break;
    case parsedUrl.protocol === 'https:' && optionsArg.keepAlive:
      optionsArg.agent = httpsAgent;
      break;
    case parsedUrl.protocol === 'https:' && !optionsArg.keepAlive:
      optionsArg.agent = httpsAgentKeepAliveFalse;
      break;
    case parsedUrl.protocol === 'http:' && optionsArg.keepAlive:
      optionsArg.agent = httpAgent;
      break;
    case parsedUrl.protocol === 'http:' && !optionsArg.keepAlive:
      optionsArg.agent = httpAgentKeepAliveFalse;
      break;
  }

  // lets determine the request module to use
  const requestModule = (() => {
    switch (true) {
      case parsedUrl.protocol === 'https:':
        return plugins.https;
      case parsedUrl.protocol === 'http:':
        return plugins.http;
    }
  })() as typeof plugins.https;

  if (!requestModule) {
    console.error(`The request to ${urlArg} is missing a viable protocol. Must be http or https`);
    return;
  }

  // lets perform the actual request
  const requestToFire = requestModule.request(optionsArg, async (resArg) => {
    if (optionsArg.hardDataCuttingTimeout) {
      setTimeout(() => {
        resArg.destroy();
        done.reject(new Error('Request timed out'));
      }, optionsArg.hardDataCuttingTimeout)
    }

    if (responseStreamArg) {
      done.resolve(resArg as IExtendedIncomingMessage);
    } else {
      const builtResponse = await buildUtf8Response(resArg, optionsArg.autoJsonParse);
      done.resolve(builtResponse);
    }
  });

  // lets write the requestBody
  if (optionsArg.requestBody) {
    if (optionsArg.requestBody instanceof plugins.formData) {
      optionsArg.requestBody.pipe(requestToFire).on('finish', (event: any) => {
        requestToFire.end();
      });
    } else {
      if (typeof optionsArg.requestBody !== 'string') {
        optionsArg.requestBody = JSON.stringify(optionsArg.requestBody);
      }
      requestToFire.write(optionsArg.requestBody);
      requestToFire.end();
    }
  } else if (requestDataFunc) {
    requestDataFunc(requestToFire);
  } else {
    requestToFire.end();
  }

  // lets handle an error
  requestToFire.on('error', (e) => {
    console.error(e);
    requestToFire.destroy();
  });

  const response = await done.promise;
  response.on('error', (err) => {
    console.log(err);
    response.destroy();
  });
  return response;
};

export const safeGet = async (urlArg: string) => {
  const agentToUse = urlArg.startsWith('http://') ? new plugins.http.Agent() : new plugins.https.Agent();
  try {
    const response = await request(urlArg, {
      method: 'GET',
      agent: agentToUse,
      timeout: 5000,
      hardDataCuttingTimeout: 5000,
      autoJsonParse: false,
    });
    return response;
  } catch (err) {
    console.log(err);
    return null;
  }
};
